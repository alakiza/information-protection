#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 14:13:16 2021

@author: alakiza
"""

def decrypt_string(text, key):
    res = ""
    
    key_size=len(key)
    for (i, ch) in enumerate(text):
        index_key=i % key_size
        if ( key[index_key] ):
            res += ch
            
    return res

if __name__ == "__main__":
    key = []
    with open("key.txt", "r") as f:
        tmp = f.readline().strip()
        for ch in tmp:
            digit = int(ch)
            if digit == 0 or digit == 1:
                key.append(digit)
            else:
                raise Exception("key must have only 0 and 1")
    
    # print(key)
    
    while True:
        try:
            source_text=input()
            
            decrypted = decrypt_string(source_text, key)
            
            print(decrypted)
        except EOFError as e:
            break
