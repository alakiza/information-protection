#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 14:13:16 2021

@author: alakiza
"""
import random

def encode_string(text, key, alphabet):
    key_size=len(key)
    alphabet_len = len(alphabet)
    
    res = ""
    i = 0
    j = 0
    for ch in text:
        
        index_key=j % key_size
        while (not key[index_key]):
            rand_char = alphabet[random.randint(0, alphabet_len-1)]
            
            res += rand_char
            
            j += 1
            index_key=j % key_size
            
        if ( key[index_key] ):
            res += ch
            
        i += 1
        j += 1
    
    return res

if __name__ == "__main__":
    random.seed()

    key = []
    with open("key.txt", "r") as f:
        tmp = f.readline().strip()
        for ch in tmp:
            digit = int(ch)
            if digit == 0 or digit == 1:
                key.append(digit)
            else:
                raise Exception("key must have only 0 and 1")
    
    # print(key)
    
    while True:
        try:
            source_text=input()
            
            alphabet = (
                        " ,.:-01234567890()абвгдежзийклмнопрстуфхцчшщъыьэюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"    
                        )
            
            # source_text = "ПриветЯтестовыйтекст"
            
            res = encode_string(source_text, key, alphabet)
        
            print(res)
        except EOFError as e:
            break
